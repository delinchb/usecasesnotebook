#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
**Description**
    This module includes the utils
        - To save results
        -
"""

import csv
import pandas as pd


def save_energy_flows(*nodes, file_name=None, sep='\t'):
    if file_name is None:
        file_name = 'energy_flows_results.csv'
    else:
        file_name += '.csv'

    time = getattr(nodes[0], 'time')

    energy_flows = [
        ['date'] + [date.to_pydatetime() for date in time.DATES] + ['hour']]
    for node in nodes:
        for energy_flow in node.get_flows:
            v = getattr(energy_flow, 'value')
            parent = getattr(energy_flow, 'parent')
            name = getattr(parent, 'name')

            if isinstance(v, list):
                energy_flows.append([name] + v)
            elif isinstance(v, dict):
                energy_flows.append([name] + list(v.values()))
            else:
                pass

    with open(file_name, 'w', newline='') as f:
        writer = csv.writer(f, delimiter=sep)
        writer.writerows(zip(*energy_flows))


def select_csv_file_between_dates(file_path=None, start='DD/MM/YYYY HH:MM',
                                  end='DD/MM/YYYY HH:MM', sep=';'):
    # Read CSV file from path and store into dataframe df
    df = pd.read_csv(file_path, sep=sep, usecols=[0, 1], header=0,
                     names=['date', 'value'])

    # Ensure that the 'date' column is at the format datetime
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)

    # Set the date as index
    df = df.set_index(['date'])
    df.sort_index()

    # Convert start and end into the format datetime
    start = pd.to_datetime(start)
    end = pd.to_datetime(end)

    # Select from start to end
    selected_df = df.loc[start: end]

    return selected_df['value']
