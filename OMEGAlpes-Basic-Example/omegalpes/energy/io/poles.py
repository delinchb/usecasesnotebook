#! usr/bin/env python3
#  -*- coding: utf-8 -*-


"""
 ** This module defines inputs and outputs of as poles**

 The poles module includes :
 - FlowPole :  this class defines a pole with a directed flow (in or out)
 - EPole : this class define an energy pole

"""


class FlowPole(dict):
    """
    Interface for basics flux poles
    """

    def __init__(self, flow='flow', direction='in'):
        """

        :param flow: name of the flow
        :type flow: str
        :param direction: direction of the flow : 'in' or 'out'
        :type direction: str
        """
        dict.__init__(self)

        self.flow = flow
        self.direction = direction


class Epole(FlowPole):
    """
    ** Description **
    Definition of an energetic pole, power and power
    flow direction convention 'in' or 'out'
    """

    def __init__(self, p, direction, energy_type=None):
        FlowPole.__init__(self, flow='p', direction='direction')
        self.energy_type = energy_type
        self.update({self.flow: p, self.direction: direction})
