#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
** This module defines the conversion units, with at least a production unit
and a consumption unit and using one or
several energy types**

 The conversion_units module defines various classes of conversion units,
 from generic to specific ones. It includes :
    - ConversionUnit : simple conversion unit. It inherits from Unit.
    - ElectricalToHeatConversionUnit : Electrical to heat Conversion unit with
 an electricity consumption and a heat
 production and consumption. It inherits from ConversionUnit
    - HeatPump : Simple Heat Pump with a theoretical coefficient of performance
 COP. It inherits from ElectricalToHeatConversionUnit, and its COP is the
 electrical to heat ratio.

 """

from .consumption_units import ConsumptionUnit
from .production_units import ProductionUnit
from ...general.optimisation.elements import Quantity, DynamicConstraint
from ...general.optimisation.units import Unit


class ConversionUnit(Unit):
    """
    **Description**
        Simple Conversion unit

    **Attributes**

        * prod_units : list for the production units
        * cons_units : list for the consumption units
        * owner : stakeholder how owns the consumption unit
    """

    def __init__(self, time, name, prod_units=None, cons_units=None,
                 owner=None):
        Unit.__init__(self, name=name, description='Conversion unit')

        self.time = time
        self.owner = owner  # Owner of the conversion unit
        self.prod_units = []  # Initialize an empty list for the
        # production units
        self.cons_units = []  # Initialize an empty list for the consumption
        # units
        self.poles = {}  # Initialize an empty dictionary for the poles

        # A conversion unit is created with at least a production unit and a
        # consumption unit
        if not prod_units:
            raise IndexError('You have to fill at least a production unit.')
        elif not isinstance(prod_units, list):
            raise TypeError('prod_units should be a list.')
        else:
            for prod_unit in prod_units:
                # prod_units should only contain ProductionUnit
                if not isinstance(prod_unit, ProductionUnit):
                    raise TypeError(
                        'The elements in prod_units have to be the type '
                        '"ProductionUnit".')
                else:
                    self._add_production_unit(prod_unit)

        if not cons_units:
            raise IndexError('You have to fill at least a consumption unit.')
        elif not isinstance(cons_units, list):
            raise TypeError('cons_units should be a list.')
        else:
            for cons_unit in cons_units:
                # cons_units should only contain ConsumptionUnit
                if not isinstance(cons_unit, ConsumptionUnit):
                    raise TypeError(
                        'The elements in cons_units have to be the type '
                        '"ConsumptionUnit".')
                else:
                    self._add_consumption_unit(cons_unit)

    def _add_production_unit(self, prod_unit):
        """

        :param prod_unit:
        """
        if prod_unit not in self.prod_units:
            poles_nb = len(self.poles)
            self.poles[poles_nb + 1] = prod_unit.poles[1]
            self.prod_units.append(prod_unit)
            prod_unit.parent = self

    def _add_consumption_unit(self, cons_unit):
        """

        :param cons_unit:
        """
        if cons_unit not in self.cons_units:
            poles_nb = len(self.poles)
            self.poles[poles_nb + 1] = cons_unit.poles[1]
            self.cons_units.append(cons_unit)
            cons_unit.parent = self


class ElectricalToHeatConversionUnit(ConversionUnit):
    """
    **Description**
        Electrical to heat Conversion unit with an electricity consumption
        and a heat production and consumption

    **Attributes**

        * pmin_in_elec : minimal incoming electrical power
        * pmax_in_elec : maximal incoming electrical power
        * p_in_elec : power input for the electrical consumption unit
        * pmin_in_heat : minimal incoming thermal power
        * pmax_in_heat : maximal incoming thermal power
        * p_in_heat : power input for the heat consumption unit
        * pmin_out : minimal power output (heat)
        * pmax_out : maximal power output (heat
        * p_out : power output
        * elec_to_heat ratio : electricity to heat ratio
        * losses : losses as a percentage of Pheat produced (p_out)
    """

    def __init__(self, time, name, pmin_in_elec=1e-5, pmax_in_elec=1e+5,
                 p_in_elec=None, pmin_in_heat=1e-5, pmax_in_heat=1e+5,
                 p_in_heat=None, pmin_out=1e-5, pmax_out=1e+5, p_out=None,
                 elec_to_heat_ratio=1, losses=0, owner=None):

        self.heat_production_unit = ProductionUnit(time, name + '_heat_prod',
                                                   p=p_out, energy_type='Heat',
                                                   p_min=pmin_out,
                                                   p_max=pmax_out, owner=owner)
        self.heat_consumption_unit = ConsumptionUnit(time, name + '_heat_cons',
                                                     p_min=pmin_in_heat,
                                                     p_max=pmax_in_heat,
                                                     p=p_in_heat,
                                                     energy_type='Heat',
                                                     owner=owner)
        self.elec_consumption_unit = ConsumptionUnit(time, name + '_elec_cons',
                                                     p_min=pmin_in_elec,
                                                     p_max=pmax_in_elec,
                                                     p=p_in_elec,
                                                     energy_type='Electrical',
                                                     owner=owner)

        ConversionUnit.__init__(self, time, name,
                                prod_units=[self.heat_production_unit],
                                cons_units=[self.heat_consumption_unit,
                                            self.elec_consumption_unit],
                                owner=owner)

        if isinstance(elec_to_heat_ratio, (int, float)):
            self.conversion = DynamicConstraint(
                exp_t='{0}_p[t] == {1} * {2}_p[t]'.format(
                    self.heat_production_unit.name,
                    elec_to_heat_ratio,
                    self.elec_consumption_unit.name),
                t_range='for t in time.I', name='conversion', parent=self)

            if elec_to_heat_ratio >= 1:
                self.power_flow = DynamicConstraint(
                    exp_t='{0}_p[t]*(1+{1}) == {2}_p[t] + {3}_p[t]'
                    .format(self.heat_production_unit.name, losses,
                            self.heat_consumption_unit.name,
                            self.elec_consumption_unit.name),
                    t_range='for t in time.I',
                    name='power_flow', parent=self)
            else:
                # TODO
                pass

        elif len(elec_to_heat_ratio) == self.time.LEN:
            # TODO : Check if >= 1
            self.conversion = DynamicConstraint(
                exp_t='{0}_p[t] == {1}[t] * {2}_p[t]'.format(
                    self.heat_production_unit.name,
                    elec_to_heat_ratio,
                    self.elec_consumption_unit.name),
                t_range='for t in time.I', name='conversion', parent=self)
            self.power_flow = DynamicConstraint(
                exp_t='{0}_p[t]*(1+{1}) == {2}_p[t] + {3}_p[t]'
                .format(self.heat_production_unit.name, losses,
                        self.heat_consumption_unit.name,
                        self.elec_consumption_unit.name),
                t_range='for t in time.I',
                name='power_flow', parent=self)
        else:
            raise IndexError(
                "Electricity to heat ratio should be a mean value or a vector "
                "for each time period !")


class HeatPump(ElectricalToHeatConversionUnit):
    """
    ** Description **
        Simple Heat Pump with a theoretical coefficient of performance COP
    """

    def __init__(self, time, name, pmin_in_elec=1e-5, pmax_in_elec=1e+5,
                 p_in_elec=None, pmin_in_heat=1e-5, pmax_in_heat=1e+5,
                 p_in_heat=None, pmin_out=1e-5, pmax_out=1e+5, p_out=None,
                 COP=3., losses=0, owner=None):
        ElectricalToHeatConversionUnit.__init__(self, time, name, pmin_in_elec,
                                                pmax_in_elec, p_in_elec,
                                                pmin_in_heat,
                                                pmax_in_heat, p_in_heat,
                                                pmin_out, pmax_out, p_out,
                                                elec_to_heat_ratio=COP,
                                                losses=losses,
                                                owner=owner)

        # Energy conversion between consumption and production
        self.COP = Quantity(name='COP', opt=False, value=COP, parent=self)
