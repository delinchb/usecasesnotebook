# imports for storage design 
from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit 
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.plots import plot_node_energetic_flows
from omegalpes.general.utils import save_energy_flows
from omegalpes.general.time import TimeUnit

# imports for optimization of the production
#! usr/bin/env python3
#  -*- coding: utf-8 -*-
from omegalpes.general.plots import plot_quantity, plt

def storage(load_profile, production_pmax, storage_pcharge_max,
         storage_pdischarge_max):
    """
    :param production_pmax: Maximal power delivered by the production unit [kW]
    :type production_pmax: float or int
    :param storage_pcharge_max: Maximal charging power for the storage [kW]
    :type storage_pcharge_max: float or int
    :param storage_pdischarge_max: Maximal discharging power for the storage
    [kW]
    :type storage_pdischarge_max: float or int
    :param load_profile: hourly load profile during a day
    :type load_profile: list of 24 float or int

    """

    global model, time, load, production, storage, node

    # Create an empty model
    model = OptimisationModel(name='example')  # Optimisation model
    time = TimeUnit(periods=24, dt=1)  # Study on a day (24h), delta_t = 1h

    # Create the load - The load profile is known
    load = FixedConsumptionUnit(time, 'load', p=load_profile)

    # Create the production unit - The production profile is unknown
    production = VariableProductionUnit(time, 'production',
                                        pmax=production_pmax)

    # Create the storage
    storage = StorageUnit(time, name='storage', pc_max=storage_pcharge_max,
                          pd_max=storage_pdischarge_max)
    storage.minimize_capacity()  # Minimize the storage capacity

    # Create the energy node and connect units
    node = EnergyNode(time, 'energy_node')

    # Add the energy node to the model
    node.connect_units(load, production,
                       storage)  # Connect all units on the same energy node
    model.add_nodes(node)  # Add node to the model

    # Optimisation process
    model.writeLP('examples\simple_storage_design.lp')
    model.solve_and_update()

def print_results_storage():
    """
        *** This function print the optimisation result:
                - The optimal capacity

            And plot the power curves :
                - Power consumed by the storage, labelled 'Storage'
                - Power consumed by the load, labelled 'Load'
                - Power delivered by the production unit, labelled 'Production'
    """

    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        print(("The optimal storage capacity is {0} kWh".format(
            storage.capacity)))

        # Show the graph
        plt = plot_node_energetic_flows(node)
        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem is infeasible !")
        
        
