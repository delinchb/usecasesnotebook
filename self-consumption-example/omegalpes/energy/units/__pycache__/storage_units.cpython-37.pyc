B
    m,�[A  �               @   sl   d Z ddlmZ ddlmZ ddlmZmZmZ ddlm	Z	 ddlm
Z
 G d	d
� d
e�ZG dd� de�ZdS )a�  
** This module defines the storage units**

 The storage_units module defines various kinds of storage units with
 associated attributes and methods, from simple to specific ones. It includes :
    - StorageUnit : simple storage unit inheriting from EnergyUnit,
     with storage specific attributes. It includes the objective "minimize
     capacity".
    - Thermocline storage : a thermal storage that need to cycle (i.e.
    reach SOC_max) every period of Tcycle
�    )�LpBinary�   )�
EnergyUnit�   )�DynamicConstraint�ExternalConstraint�ExtDynConstraint)�	Objective)�Quantityc               @   s$   e Zd ZdZdd	d
�Zddd�ZdS )�StorageUnita_  
    **Description**
        Simple Storage unit

    **Attributes**
        * capacity (Quantity): maximal energy that can be stored
        * e (Quantity): energy at time t in the storage
        * set_soc_min (DynamicConstraint): constraining the energy to be
        above the value : soc_min*capacity
        * set_soc_max (DynamicConstraint): constraining the energy to be
        below the value : soc_max*capacity
        * pc (Quantity) : charging power
        * pd (Quantity) : discharging power
        * u_c (Quantity) : binary variable describing the charge of the
        storage unit : 0 : Not charging & 1 : charging
        * calc_e (DynamicConstraint) : energy calculation at time t ;
        relation power/energy
        * calc_p (DynamicConstraint) : power calculation at time t ; power
        flow equals charging power minus discharging power
        * on_off_stock (DynamicConstraint) : making u[t] matching with
        storage modes (on/off)
        * def_max_charging (DynamicConstraint) : defining the max charging
        power, avoiding charging and discharging at the same time
        * def_max_discharging (DynamicConstraint) : defining the max
        discharging power, avoiding charging and discharging at the same time
        * def_min_charging (DynamicConstraint) : defining the min charging
        power, avoiding charging and discharging at the same time
        * def_min_discharging (DynamicConstraint) : defining the min
        discharging power, avoiding charging and discharging at the same time
        * set_e_0 (ExternalConstraint) : set the energy state for t=0
        * set_e_f (ExternalConstraint) : set the energy state for the last time step
        * ef_is_e0 (ExternalConstraint) : Imposing ef=e0 on the time period.
        * cycles (ExternalDynamicConstraint) : setting a cycle constraint
        e[t] = e[t+cycles/dt]

    �StU1��h㈵��>�     j�@Nr   r   Fc             C   s:  t j| ||dd| d|||d�
 |dkr.d}n|
dk	rH|
|krLtd��nd}
tdd	|dd| d
�| _tdddd	|jd|| d�| _t|
tt	f�r�t
d�| j|
�d| d�| _n$t|
t�r�t
d�| j|
�d| d�| _t|tt	f�r�t
d�| j|�d| d�| _n&t|
t��rt
d�| j|�d| d�| _tddd|jd|| d�| _tddd|jd|| d�| _tdt|jdd| d�| _d|  k�r�dk�r�n n"t
ddd�| j|||�| d �| _ntd!�|���t
d"�| j�d#d$d%�| _t
d&j| jd'd(�d#d)| d*�| _t
d+�| j|�d#d,| d*�| _t
d-�| j|�d#d.| d*�| _t
d/�| j|�d#d0| d*�| _t
d1�| j|�d#d2| d*�| _|dk	�r~td3d4�| j|�| d5�| _|	dk	�r�td6d7�| j|	�| d5�| _|�r�|	dk�s�|dk�s�|	|k�r�td8�| j�d9| d:�| _ntd;��|dk	�r6t|�tk�r.||j  }t!d<d=�| j|�d>�|�| d?�| _"nt#d@��dS )AaP  
        :param time: TimeUnit describing the studied time period
        :param name: name of the storage unit
        :param pc_min: minimal charging power [kW]
        :param pc_max: maximal charging power [kW]
        :param pd_min: minimal discharging power [kW]
        :param pd_max: maximal discharging power [kW]
        :param capacity: maximal energy that can be stored [kWh]
        :param e_0: initial level of energy [kWh]
        :param e_f: final level of energy [kWh]
        :param soc_min: minimal state of charge [pu]
        :param soc_max: maximal state of charge [pu]
        :param eff_c: charging efficiency
        :param eff_d: discharging efficiency
        :param self_disch: part of the soc that is self-discharging [pu]
        :param ef_is_e0: binary describing whether the storage is working at
        constant energy during the entire time period (e_0=e_f) or not.
        :param cycles: number of hours between cycling (e[t] = e[t+cycles/dt]
        :param energy_type: energy type the storage unit is used with
        :param owner: owner of the storage unit
        �inNg     j��)�flow_direction�p�p_min�e_min�p_max�energy_type�ownerr   z.You cannot have soc_min > soc_max / capacity !r   �capacity�kWh)�name�unit�value�lb�vlen�parent�eTzenergy at t in the storage)r   �opt�descriptionr   r   r   �ubr   z{0}_e[t] >= {1} * {0}_capacity�set_soc_min)�exp_tr   r   z!{0}_e[t] >= {1}[t] * {0}_capacityz{0}_e[t] <= {1} * {0}_capacity�set_soc_maxz!{0}_e[t] <= {1}[t] * {0}_capacity�pc�kW)r   r    r   r   r   r"   r   �pd�ucz*binary variable 0:No charging & 1:charging)r   �vtyper   r    r!   r   �calc_ez for t in time.I[:-1]z]{0}_e[t+1] - {0}_e[t] + {1} * {0}_capacity + ({0}_pd[t]*1/{2} - {0}_pc[t]*{3}) * time.DT == 0)r   �t_ranger$   r   z}self_disch is the part of the soc that is automatically discharging, it should have a value between 0 and 1 and is set to {0}z!{0}_p[t] == {0}_pc[t] - {0}_pd[t]zfor t in time.I�calc_p)r$   r,   r   z-{0}_pc[t] + {0}_pd[t] - {0}_u[t] * {eps} >= 0g����MbP?)�eps�on_off_stock)r$   r,   r   r   z {0}_pc[t] - {0}_uc[t] * {1} <= 0�def_max_chargingz&{0}_pd[t] - (1 - {0}_uc[t]) * {1} <= 0�def_max_dischargingz {0}_pc[t] - {0}_uc[t] * {1} >= 0�def_min_chargingz-{0}_pd[t] + ({0}_uc[t] - {0}_u[t]) * {1} >= 0�def_min_discharging�set_e_0z{0}_e[0] == {1})r   �expr   �set_e_fz{0}_e[time.I[-1]] == {1}z{0}_e[0] == {0}_e[time.I[-1]]�ef_is_e0)r5   r   r   zBWhen ef_is_e0 is set to True, e_f or e_0 should remain set to None�
set_cyclesz{0}_e[t] == {0}_e[t+{1}]zfor t in time.I[:-{0}])r   r$   r,   r   zTcycles should be an integer : number of hours between cycling (e[t] = e[t+cycles/dt])$r   �__init__�
ValueErrorr
   r   �LENr   �
isinstance�int�floatr   �formatr   r#   �listr%   r&   r(   r   r)   r+   r-   r/   r0   r1   r2   r3   r   r4   r6   r7   �type�DTr   r8   �	TypeError)�self�timer   �pc_min�pc_max�pd_min�pd_maxr   �e_0�e_f�soc_min�soc_max�eff_c�eff_d�
self_dischr7   Zcyclesr   r   Zdelta_t� rQ   �8D:\dev\Omegalpes\omegalpes\energy\units\storage_units.pyr9   ?   s�    













zStorageUnit.__init__c             C   sL   t d�| j�dd| d�}tdd�| j�|| d�}t| d|� t| d|� dS )	zF

        :param weight: Weight coefficient for the objective
        z{0}_e[t] <= {0}_capacityzfor t in time.I�def_capacity)r$   r,   r   r   �min_capacityz{0}_capacity)r   r5   �weightr   N)r   r?   r   r	   �setattr)rD   rU   rS   rT   rQ   rQ   rR   �minimize_capacity�   s    

zStorageUnit.minimize_capacity)r   r   r   r   r   NNNr   Nr   r   r   FNNN)r   )�__name__�
__module__�__qualname__�__doc__r9   rW   rQ   rQ   rQ   rR   r      s   $    
 2r   c               @   s   e Zd ZdZdd
d�ZdS )�ThermoclineStorageaK  
    **Description**

        Class ThermoclineStorage : class defining a thermocline heat storage,
        inheriting from StorageUnit.

    **Attributes**
        * is_soc_max (Quantity) : indicating if the storage is fully charged
        0:No 1:Yes
        * def_is_soc_max_inf (DynamicConstraint) : setting the right value
        for is_soc_max
        * def_is_soc_max_sup (DynamicConstraint) : setting the right value
        for is_soc_max
        * force_soc_max (ExtDynConstraint) : The energy has to be at least
        once at its maximal value during the period Tcycl.
    �Thermocline��h㈵��>�     j�@Nr   r   �x   Fc             C   s�   t j| |||||||||	|
|||||d|d� tdd|jtd| d�| _d}td�| j|�d	d
| d�| _	td�| j�d	d| d�| _
td�| jt||j ��d�t||j ��d| d�| _dS )a�  
        :param time: TimeUnit describing the studied time period
        :param name: name of the storage unit
        :param pc_min: minimal charging power [kW]
        :param pc_max: maximal charging power [kW]
        :param pd_min: minimal discharging power [kW]
        :param pd_max: maximal discharging power [kW]
        :param capacity: maximal energy that can be stored [kWh]
        :param e_0: initial level of energy [kWh]
        :param e_f: final level of energy [kWh]
        :param soc_min: minimal state of charge [pu]
        :param soc_max: maximal state of charge [pu]
        :param eff_c: charging efficiency
        :param eff_d: discharging efficiency
        :param self_disch: part of the soc that is self-discharging [pu]
        :param Tcycl: period over which the storage is cycling (reaching at
        least once it max state of charge) [hours]
        :param ef_is_e0: binary describing whether the storage is working at
        constant energy during the entire time period (e_0=e_f) or not.
        :param cycles: number of hours between cycling (e[t] = e[t+cycles/dt]
        :param owner: owner of the storage unit
        ZHeat)r   rF   rG   rH   rI   r   rJ   rK   rL   rM   rN   rO   rP   r7   r   r   �
is_soc_maxTz4indicates if the storage is fully charged 0:No 1:Yes)r   r    r   r*   r!   r   g�������?zC{0}_capacity * {0}_is_soc_max[t] >= ({0}_e[t] - {0}_capacity + {1})zfor t in time.I�def_is_soc_max_inf)r$   r,   r   r   z,{0}_capacity * {0}_is_soc_max[t] <= {0}_e[t]�def_is_soc_max_supz8lpSum({0}_is_soc_max[k] for k in range(t-{1}+1, t)) >= 1zfor t in time.I[{0}:]�force_soc_maxN)r   r9   r
   r;   r   ra   r   r?   r   rb   rc   r   �roundrB   rd   )rD   rE   r   rF   rG   rH   rI   r   rJ   rK   rL   rM   rN   rO   rP   ZTcyclr7   r   �epsilonrQ   rQ   rR   r9     s0    

zThermoclineStorage.__init__)r]   r^   r_   r^   r_   NNNNNr   r   r   r`   FN)rX   rY   rZ   r[   r9   rQ   rQ   rQ   rR   r\     s       r\   N)r[   �pulpr   �energy_unitsr   Zgeneral.optimisation.elementsr   r   r   r	   r
   r   r\   rQ   rQ   rQ   rR   �<module>   s    p