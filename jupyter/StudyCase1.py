from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
from omegalpes.general.plots import plot_quantity, plt

def production_optim(op_cost_a, op_cost_b, consumption_file):
    
    global time, model, dwelling_consumption, grid_production_A, \
        grid_production_B
    """
    :param op_cost_a: operating costs for the energy unit A [€]
    :type op_cost_a: list of 24 float or int
    :param op_cost_b: operating costs for the energy unit B [€]
    :type op_cost_b: list of 24 float or int
    :param consumption_file: link to the data file
    :type consumption_file: string

    """

    # Creating an empty model
    model = OptimisationModel(name='elec_prod_simple_example')

    # Creating the unit dedicated to time management, and importing
    # time-dependent data
    time = TimeUnit(periods=24, dt=1)
    building_cons_file = open(consumption_file, "r")

    # Creating the dwelling consumption - the load profile is known
    electrical_load = [c for c in map(float, building_cons_file)]
    dwelling_consumption = FixedConsumptionUnit(time, 'dwelling_consumption',
                                                energy_type='Electrical',
                                                p=electrical_load)

    # Creating the production units - the production profile are unknown
    grid_production_A = VariableProductionUnit(time=time,
                                               name='grid_production_A',
                                               energy_type='Electrical',
                                               operating_cost=op_cost_a)

    grid_production_B = VariableProductionUnit(time=time,
                                               name='grid_production_B',
                                               energy_type='Electrical',
                                               operating_cost=op_cost_b)

    # Objective : Minimizing the part of the electrical load covered by the
    # electrical production plants
    grid_production_A.minimize_operating_cost()
    grid_production_B.minimize_operating_cost()

    # Creating the energy nodes and connecting units
    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')
    elec_node.connect_units(dwelling_consumption, grid_production_A,
                            grid_production_B)

    # Adding the energy node to the model
    model.add_nodes(elec_node)

    # Optimisation process
    model.writeLP('elec_prod_simple_example.lp')
    model.solve_and_update()  # Run optimization and update values

    return model, time, dwelling_consumption, grid_production_A, \
        grid_production_B

def print_results_optimization():
    """
        *** This function prints the optimisation result:
                - The dwelling consumption
                - The grid_production A
                - The grid_production B

            Then plots the power curves :
                - Consumption form the dwelling load, labelled 'dwelling
                consumption'
                - grid_productions A and B, labelled 'grid_production A' and
                'grid_production B'
    """
    
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('Dwelling consumption = {0} kWh.'.format(dwelling_consumption.e_tot))
        print('grid_production A production = {0} kWh'.format(
            grid_production_A.e_tot))
        print('grid_production B production = {0} kWh'.format(
            grid_production_B.e_tot))

        # Plot the figures
        fig1 = plt.figure(1)
        ax1 = plt.axes()
        legend1 = []

        plot_quantity(time, dwelling_consumption.p, fig1, ax1)
        legend1 += ['Dwelling consumption']

        plt.legend(legend1)

        fig2 = plt.figure(2)
        ax2 = plt.axes()
        legend2 = []

        plot_quantity(time, grid_production_A.p, fig2, ax2)
        legend2 += ['grid production A']

        plot_quantity(time, grid_production_B.p, fig2, ax2)
        legend2 += ['grid production B']

        plt.legend(legend2)

        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")