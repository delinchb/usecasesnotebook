OMEG'ALPES Lib for linear energy systems modelling
==================================================

OMEG'ALPES stands for Generation of Optimization Models As Linear Programming for Energy Systems.
OMEG'ALPES aims to be an energy systems modelling tool for linear optimisation (LP, MILP).


This code is under the licence :


Please install OMEG'ALPES Lib with pip:
    pip install + gitlab link


Project Presentation
--------------------
Please have a look to the html presentation of the project : docs\_build\html\index.html
Just copy the link in the address bar in the folder OMEGALPES


Installation Requirements
-------------------------
Python == 3.6.0
PuLP == 1.6.8
Matplotlib == 2.2.2
Seaborn == 0.8.1
Numpy == 1.14.2
Pandas == 0.22.0
Python_dateutils == 2.7.3
