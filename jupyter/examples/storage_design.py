#! usr/bin/env python3
#  coding=utf-8 #

"""
    ** This Module is an example of storage capacity optimisation.**

        This example describes a simple microgrid with :
            - A load profile known in advance
            - An adjustable production unit, with a maximum power limit
            - A storage system with power in charge and power in discharge

        The objective consists on minimizing the storage capacity.
"""
import os

from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.plots import plt, plot_quantity, \
    plot_node_energetic_flows
from omegalpes.general.utils import save_energy_flows
from omegalpes.general.time import TimeUnit


def main(work_path, load_profile, production_pmax, storage_pcharge_max,
         storage_pdischarge_max):
    """
    :param production_pmax: Maximal power delivered by the production unit [kW]
    :type production_pmax: float or int
    :param storage_pcharge_max: Maximal charging power for the storage [kW]
    :type storage_pcharge_max: float or int
    :param storage_pdischarge_max: Maximal discharging power for the storage
    [kW]
    :type storage_pdischarge_max: float or int
    :param load_profile: hourly load profile during a day
    :type load_profile: list of 24 float or int

    """

    # Create an empty model
    model = OptimisationModel(name='example')  # Optimisation model
    time = TimeUnit(periods=24, dt=1)  # Study on a day (24h), delta_t = 1h

    # Create the load - The load profile is known
    load = FixedConsumptionUnit(time, 'load', p=load_profile)

    # Create the production unit - The production profile is unknown
    production = VariableProductionUnit(time, 'production',
                                        pmax=production_pmax)

    # Create the storage
    storage = StorageUnit(time, name='storage', pc_max=storage_pcharge_max,
                          pd_max=storage_pdischarge_max, soc_min=0.1,
                          soc_max=0.9, self_disch=0.01, ef_is_e0=True)
    storage.minimize_capacity()  # Minimize the storage capacity

    # Create the energy node and connect units
    node = EnergyNode(time, 'energy_node')

    # Add the energy node to the model
    node.connect_units(load, production,
                       storage)  # Connect all units on the same energy node
    model.add_nodes(node)  # Add node to the model

    # Optimisation process
    model.writeLP(work_path + r'\simple_storage_design.lp')
    model.solve_and_update()

    return model, time, load, production, storage, node


def print_results():
    """
        *** This function print the optimisation result:
                - The optimal capacity

            And plot the power curves :
            Figure 1 :
                - Power consumed by the storage, labelled 'Storage'
                - Power consumed by the load, labelled 'Load'
                - Power delivered by the production unit, labelled 'Production'
            Figure 2 :
                - The state of charge of the storage unit
    """

    if LpStatus[MODEL.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        print("The optimal storage capacity is {0} kWh".format(
            STORAGE.capacity))

        # Show the graph
        # Power curves
        plot_node_energetic_flows(NODE)
        # SOC curve
        plot_quantity(TIME, STORAGE.e)
        plt.xlabel('Time (h)')
        plt.ylabel('Energy (kWh)')
        plt.title('State of charge of the storage system')
        plt.show()

    elif LpStatus[MODEL.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[MODEL.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve.")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == '__main__':
    # OPTIMIZATION PARAMETERS #
    WORK_PATH = os.getcwd()

    # Load dynamic profile - one value per hour during a day
    LOAD_PROFILE = [4, 5, 6, 2, 3, 4, 7, 8, 13, 24, 18, 16, 17, 12, 20, 15, 17,
                    21, 25, 23, 18, 16, 13, 4]

    # Maximal power that can be delivered by the production unit
    PRODUCTION_P_MAX = 15

    # Storage maximal charging and discharging powers
    (STORAGE_PC_MAX, STORAGE_PD_MAX) = 20, 20

    # Run main
    MODEL, TIME, LOAD, PRODUCTION, STORAGE, NODE = \
        main(work_path=WORK_PATH, load_profile=LOAD_PROFILE,
             production_pmax=PRODUCTION_P_MAX,
             storage_pcharge_max=STORAGE_PC_MAX,
             storage_pdischarge_max=STORAGE_PD_MAX)

    # Save energy flows into a CSV file
    save_energy_flows(NODE, file_name=WORK_PATH + r'\results\storage_design')

    # Show results
    print_results()
