#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
     ** This Module is an example of waste energy recovery **

    An electro-intensive industrial process consumes electricity and rejects
    heat. This waste energy is whether recovered or dissipated depending on
    the waste recovery system sizing. A storage system and a heat pump are
    used in order to recover the waste energy, which is then injected on a
    district heat network to provide heat to a district heat load. The
    missing heat will be provided by a district heat network production unit.

    The system includes :
     - An electro-intensive industry
     - A dissipation load
     - A thermal storage system
     - A heat pump
     - A district heat network load
     - A district heat network production unit

     The objective consists in maximizing the recovered waste energy
"""

from pulp import LpStatus
import os
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToHeatConversionUnit, HeatPump
from omegalpes.energy.units.production_units import ProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
from omegalpes.general.plots import plt, plot_node_energetic_flows


def main(work_path, elec2heat_ratio=0.9, pc_max=5000, pd_max=5000, pc_min=1000,
         pd_min=1000, e_max=20000, cop_hp=3, pmax_elec_hp=1000,
         storage_soc_0=0.2):

    # OPTIMIZATION MODEL
    # Creating an empty model
    model = OptimisationModel(name='waste_e_recovery_model')

    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24 * 7, dt=1)

    # Importing time-dependent data from files
    indus_cons_file = open(work_path + "/data/indus_cons_week.txt", "r")
    heat_load_file = open(work_path +
                           "/data/District_heat_load_consumption.txt", "r")

    # Creating the electro-intensive industry unit
    indus_cons = [c for c in map(float, indus_cons_file)]
    indus = ElectricalToHeatConversionUnit(time, 'indus', pmax_in_heat=1,
                                           elec_to_heat_ratio=elec2heat_ratio,
                                           p_in_elec=indus_cons)

    # Creating unit for heat dissipation from the industrial process
    dissipation = VariableConsumptionUnit(time, 'dissipation',
                                          energy_type='Heat')

    # Creating the thermal storage
    thermal_storage = StorageUnit(time, 'thermal_storage', pc_max=pc_max,
                                  pd_max=pd_max, pc_min=pc_min, pd_min=pd_min,
                                  capacity=e_max, e_0=storage_soc_0 * e_max)

    # Creating the heat pump
    heat_pump = HeatPump(time, 'heat_pump', COP=cop_hp,
                         pmax_in_elec=pmax_elec_hp)

    # Creating the district heat load
    heat_load = [c for c in map(float, heat_load_file)]
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=heat_load, energy_type='Heat')

    # Creating the heat production plants
    heat_production = ProductionUnit(time, name='heat_production',
                                     energy_type='Heat')

    # Creating the heat node for the energy flows
    heat_node_bef_valve = EnergyNode(time, 'heat_node_bef_valve',
                                     energy_type='Heat')
    heat_node_aft_valve = EnergyNode(time, 'heat_node_aft_valve',
                                     energy_type='Heat')
    heat_node_aft_hp = EnergyNode(time, 'heat_node_aft_hp', energy_type='Heat')

    # Connecting units to the nodes
    heat_node_bef_valve.connect_units(indus.heat_production_unit, dissipation)
    heat_node_bef_valve.export_to_node(
        heat_node_aft_valve)  # Export after the valve
    heat_node_aft_valve.connect_units(thermal_storage,
                                      heat_pump.heat_consumption_unit)

    heat_node_aft_hp.connect_units(heat_pump.heat_production_unit,
                                   heat_production, district_heat_load)

    # OBJECTIVE CREATION
    # Minimizing the part of the heat load covered by the heat production plant
    heat_production.minimize_production()

    # Adding all nodes (and connected units) to the optimization model
    model.add_nodes(heat_node_bef_valve, heat_node_aft_valve, heat_node_aft_hp)

    model.writeLP(work_path + r'\waste_e_recovery.lp')
    # Writing into lp file
    model.solve_and_update()  # Running optimization and update values

    return model, time, indus, district_heat_load, heat_pump, \
           heat_production, dissipation,  thermal_storage,  \
           heat_node_bef_valve, heat_node_aft_valve, \
        heat_node_aft_hp


def print_results():
    """
        *** This function prints the optimisation result:
                - The district consumption during the year
                - The industry consumption during the year
                - The district heat network production during the year
                - The heat exported from the industry
                - The rate of the load covered by the industry

            And plots the power curves :
            On the first figure : the energy out of the industry with the
            recovered and the dissipated parts
            On the second figure: the energy on the district heating network
            with the part produced by the heat pump and
            the part produced by the district heating production unit.

    """

    # Print results
    if LpStatus[MODEL.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('District consumption = {0} kWh.'.format(
            DISTRICT_HEAT_LOAD.e_tot))
        print('Industry consumption = {0} kWh.'.format(
            INDUS.elec_consumption_unit.e_tot))
        print('District heat network production = {0} kWh.'.format(
            HEAT_PRODUCTION.e_tot))
        print('Industry heat exported = {0} kWh.'.format(
            sum(
                HEAT_NODE_BEF_VALVE.energy_export_to_heat_node_aft_valve
                    .value.values())))
        print('Heat pump electricity consumption = {0} kWh.'.format(
            HEAT_PUMP.elec_consumption_unit.e_tot))
        print("{0} % of the load coming from the industry".format(
            round(sum(
                HEAT_NODE_BEF_VALVE.energy_export_to_heat_node_aft_valve
                    .value.values()) /
                  DISTRICT_HEAT_LOAD.e_tot.value * 100)))  # value is a dict,
        # with time as a key, and power levels as values.

        # SHOW THE GRAPH
        # Recovered and dissipated heat
        plot_node_energetic_flows(HEAT_NODE_BEF_VALVE)

        # Energy on the recovery system
        plot_node_energetic_flows(HEAT_NODE_AFT_VALVE)

        # Energy on the district heating network
        plot_node_energetic_flows(HEAT_NODE_AFT_HP)

        plt.show()

    elif LpStatus[MODEL.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[MODEL.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == "__main__":
    # *** OPTIMIZATION PARAMETERS *** #
    WORK_PATH = os.getcwd()
    # --- Electricity-to-Heat conversion ---
    # 90% of the electrical consumption is converted into heat
    ELEC_TO_HEAT_RATIO = 0.9

    # --- Thermal storage parameters ---
    # The maximal charging and discharging powers both equal 5 MW
    PC_MAX_STORAGE = PD_MAX_STORAGE = 5000

    # When charging/discharging, the power should at least be 20% of the
    # maximal charging/discharging powers
    PC_MIN_STORAGE = PD_MIN_STORAGE = 0.15 * PC_MAX_STORAGE

    CAPA_STORAGE = 20000    # Storage capacity of 20MWh
    SOC_0_STORAGE = 0.2    # Initial state of charge of 25%

    # --- Heat pump parameters ---
    COP = 3            # The coefficient of performance equals 3
    P_MAX_HP = 1000    # The heat pump has a electrical power limit of 1 MW

    # *** RUN MAIN ***
    MODEL, TIME, INDUS, DISTRICT_HEAT_LOAD, HEAT_PUMP, HEAT_PRODUCTION, \
    DISSIPATION, THERMAL_STORAGE, HEAT_NODE_BEF_VALVE, HEAT_NODE_AFT_VALVE, \
    HEAT_NODE_AFT_HP= main(
        work_path=WORK_PATH, elec2heat_ratio=ELEC_TO_HEAT_RATIO,
                              pc_max=PC_MAX_STORAGE,
         pd_max=PD_MAX_STORAGE, pc_min=PC_MIN_STORAGE,
         pd_min=PD_MIN_STORAGE, e_max=CAPA_STORAGE, cop_hp=COP,
         pmax_elec_hp=P_MAX_HP, storage_soc_0=SOC_0_STORAGE)

    # *** SHOW THE RESULTS ***
    print_results()
